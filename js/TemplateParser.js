
function Template(str) {

    // constructor
    this.content = str;

    // functions
    this.render = (variables) => {
        var m, name, statement, result, last, i, len, value, limit;
        result = this.content;
        limit = 255;

        // > for statements
        // get all statements
        last = 0;
        while((m = Template.regex.statement.exec(result)) !== null) {
            if (!--limit) {
                throw "Template parse gone over limit!";
            }
            name = m[1];
            if (name === 'end') {
                if (!--last) {
                    // fill last things to the statement
                    statement.innerTo = m.index;
                    statement.outerTo = m.index + m[0].length;
                    statement.innerText = result.slice(statement.innerFrom, statement.innerTo);
                    statement.outerText = result.slice(statement.outerFrom, statement.outerTo);
                    // run the statement filter and get the result back
                    filter = Template.statements[statement.name]
                    if (!filter) {
                        throw "statement '" + statement.name + "' doesn't exist";
                    }
                    // get the new result and parse the content
                    result = result.slice(0, statement.outerFrom) + (new Template(filter(statement, variables)).render(variables)) + result.slice(statement.outerTo);
                }
            } else {
                if (!last++) {
                    condition = m[3] || '';
                    statement = {
                        name: name,
                        condition: condition,
                        innerText: null,
                        innerFrom: m.index + m[0].length,
                        innerTo: null,
                        outerFrom: m.index,
                        outerTo: null
                    };
                }
            }
        }

        // > for variables
        while((m = Template.regex.variable.exec(result)) !== null) {
            value = new Template(Template.eval(m[1], variables)).render(variables);
            result = result.slice(0, m.index) + (value === undefined ? '' : value) + result.slice(m.index + m[1].length + 2);
        }

        return result;
    }
}





Template.evalValue = function(valueStr, variables = {}) {
    // initialize
    var
    value = undefined,
    operator = undefined,
    currentVar,
    previousVar,
    currentValue,
    currentFunction,
    parameters,
    names,
    name,
    len,
    i;
    // loop through every token
    while ((m = Template.regex.value.exec(valueStr)) !== null) {
        console.log(m);
        // setup values
        currentVar = m[4];
        currentValue = m[10] || m[12] || parseInt(m[7]);
        currentFunction = m[6];
        // get values
        if (undefined !== currentValue || undefined !== currentVar) {
            if (currentVar !== undefined) {
                if (currentVar == 'true') {
                    currentValue = true;
                } else if (currentVar == 'false') {
                    currentValue = false;
                } else {
                    // get variable (may be point seperated)
                    names = currentVar.replace(/\s+/g, '');
                    names = names.split('.');
                    currentVar = variables;
                    len = names.length;
                    for (i = 0; i < len; ++i) {
                        name = names[i];
                        previousVar = currentVar;
                        currentVar = currentVar[name];
                        if (currentVar === undefined) {
                            previousVar[name] = currentVar = {};
                            console.warn("variable " + m[3] + " doesn't exist");
                        }
                    }
                    // parse function if it is
                    if (undefined !== currentFunction) {
                        // split parameters and get every value of it
                        parameters = currentFunction.split(',');
                        len = parameters.length;
                        if (parameters[len-1] === "") {
                            parameters.splice(--len, 1);
                        }
                        console.log(parameters);
                        for (i = 0; i < len; ++i) {
                            parameters[i] = Template.evalValue(parameters[i], variables);
                        }
                        // call function and get the value
                        currentValue = currentVar.apply(window, parameters);
                    } else {
                        currentValue = currentVar;
                    }
                }
            }
            // eval it
            if (undefined !== operator) {
                // calculate the value
                switch (operator) {
                    // math
                    case '+':
                        value += currentValue;
                        break;
                    case '-':
                        value -= currentValue;
                        break;
                    case '*':
                        value *= currentValue;
                        break;
                    case '/':
                        value /= currentValue;
                        break;

                    // comporison
                    case '>':
                        value = value > currentValue;
                        break;
                    case '<':
                        value = value < currentValue;
                        break;
                    case '>=':
                        value = value >= currentValue;
                        break;
                    case '<=':
                        value = value <= currentValue;
                        break;
                    case '==':
                        value = value == currentValue;
                        break;
                    case '!=':
                        value = value != currentValue;
                        break;
                    case '===':
                        value = value === currentValue;
                        break;
                    case '!==':
                        value = value !== currentValue;
                        break;

                    // link
                    case '&&':
                        value = value && currentValue;
                        break;
                    case '||':
                        value = value || currentValue;
                        break;

                    // assignment
                    case '=':
                        value = (previousVar[name] = currentValue);
                        break;
                    case '+=':
                        value = (previousVar[name] += currentValue);
                        break;
                    case '-=':
                        value = (previousVar[name] -= currentValue);
                        break;
                    case '*=':
                        value = (previousVar[name] *= currentValue);
                        break;
                    case '/=':
                        value = (previousVar[name] /= currentValue);
                        break;
                }
                operator = undefined;
            }
            else if (undefined === value) {
                value = currentValue;
            }
        }
        operator = m[2] || operator;
    }
    return value;
}

Template.eval = function(code, variables) {
    var commands, len, i, result, command;
    commands = code.split(';');
    len = commands.length;
    for (i = 0; i < len; ++i) {
        command = commands[i];
        if (command === '') {
            continue;
        }
        result = Template.evalValue(command, variables);
    }
    if (len > 1) {
        return '';
    } else {
        return result;
    }
}

Template.regex = {
    variable: /\{([^]+?)\}/gi,
    statement: /\{\%\s*([A-Za-z_0-9]+)\s*(\(([^\)]*)?\))?\s*\%\}/gi,
    value: /((\+\=|\-\=|\*\=|\/\=|\+|\-|\*|\/|>|<|>=|<=|==|!=|===|!==|\=|&&|\|\|)|(([A-Za-z][A-Za-z0-9_.]*)(\s*\(\s*([^]*?)\s*\))?)|([0-9]+)|(('([^]*?)')|("([^]*?)")))/gi
};

Template.statements = {};
Template.addStatement = (name, filter) => {
    Template.statements[name] = filter;
};

Template.addStatement("if", function(statement, variables) {
    if (Template.evalValue(statement.condition, variables)) {
        return statement.innerText;
    }
    return "";
});

Template.addStatement("while", function(statement, variables) {
    var i, m, result;
    result = "";
    limit = 255;
    i = 0;
    while (Template.evalValue(statement.condition, variables) && ++i < limit) {
        result += statement.innerText;
    }
    return result;
});

Template.addStatement("repeat", function(statement, variables) {
    var i, len, result;
    result = "";
    i = 0;
    len = Template.evalValue(statement.condition, variables);
    for (; i < len; ++i) {
        result += statement.innerText;
    }
    return result;
});

Template.addStatement("foreach", function(statement, variables) {
    var i, len, result, v, key;
    result = "";
    v = Template.evalValue(statement.condition, variables);
    //if (!variables.foreach) {
        variables.foreach = {
            key: null,
            value: null
        }
    //}
    for (key in v) {
        if (typeof(v[key]) === "string") {
            v[key] = '"' + v[key] + '"';
        }
        result += "{foreach.item.key = '" + key + "'; foreach.item.value = " + v[key] + "}" + statement.innerText;
    }
    return result;
});

console.log(new Template("{var = 'bar';} foo: {var}").render());
